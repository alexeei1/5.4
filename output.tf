output "network_load_balancer_ip" {
    value = "${google_compute_forwarding_rule.loadbalancer.ip_address}"
}
