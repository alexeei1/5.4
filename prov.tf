variable project_id {}
variable email {}
variable apikey {}

terraform {
  required_providers {
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "~> 3.0"
    }
  }
}

provider "google" {
  project = var.project_id
  region = "europe-north1"
  zone = "europe-north1-a"
}

provider "cloudflare" {
  api_key = var.apikey
  email = var.email
}
